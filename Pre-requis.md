# Pré-requis

## Questions

* Décrivez brievement votre poste
* Quel éditeur de texte ou environnement de développement vous utilisez.
* Quel système d'exploitation vous utilisez (et sa version).
* Comment vous vous servez de git : en ligne de commande ? avec un outil tiers ? (si oui lequel ?)

## Préparatifs techniques

* se [créer un compte sur Adullact.net](https://adullact.net/account/register.php)
* se connecter sur Adullact.net (note: la confirmation du compte n'est pas une connexion, il faut bien se connecter après avoir confirmé son compte).

Sur son poste de travail windows :

* [Télécharger Git](https://git-scm.com/download/win) et l'installer
* Créer une clée SSH avec Git Bash : `ssh-keygen -t rsa -b 4096` (conserver les chemins proposés par défaut)
* Se connecter sur [gitlab.adullact.net](http://gitlab.adullact.net/)
* Suivre les instruction pour ajouter sa clé **publique** SSH (la clée étant créé, il n'y a plus qu'à copier la partie publique à l'emplacement dédié dans le Gitlab)
* Vérifier lançant depuis le *git bash* la commande `git clone git@gitlab.adullact.net:adullact/Accompagnement-git-gitlab.git`

## Proxy

Pour configurer un proxy pour git il convient de taper :

```
git config --global http.proxy http://proxyuser:proxypwd@proxy.server.com:proxyport
```

Exemple : `git config --global http.proxy http://mfaure:motdepasse@proxy.adullact.org:8080`